package com.mdev.spleattest.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mdev.spleattest.R;
import com.mdev.spleattest.collection.Data;
import com.mdev.spleattest.commons.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * @author Miral Desai on 04/2015.
 */
public class ListAdapter extends BaseAdapter {

    private ImageLoader imageLoader;
    DisplayImageOptions options;
    Context context;
    ArrayList<Data> objects;
    double latitude, longitude;

    public ListAdapter(Context context, ArrayList<Data> objects, double latitude, double longitude) {
        super();
        this.context = context;
        this.objects = objects;
        this.latitude = latitude;
        this.longitude = longitude;

        //Allows user to know exactly what happened if image failed to load, and doesn't mean they all fail.
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_launcher) //Very handy for showing user a placeholder while real image loads
                .showImageOnFail(R.mipmap.ic_launcher) //If image fails to load, show error image
                .considerExifParams(true)
                .cacheInMemory(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Typeface rLight = Typeface.createFromAsset(context.getAssets(), Constants.RLIGHT);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, parent, false);
        }

        //Create location from lat long of user
        Location userLocation = new Location("");
        userLocation.setLatitude(latitude);
        userLocation.setLongitude(longitude);

        //Create location from lat long of venue
        Location venueLocation = new Location("");
        venueLocation.setLatitude(objects.get(position).getLatitude());
        venueLocation.setLongitude(objects.get(position).getLongitude());

        //Calculate distance from user location to venue in KM
        double distanceBetween = userLocation.distanceTo(venueLocation)/1000; //Will show in metres if I remove /1000

        //Round to nearest whole number to avoid point values
        long roundedDistance = Math.round(distanceBetween);

        TextView distance = (TextView) convertView.findViewById(R.id.venue_distance);
        distance.setText(roundedDistance+"km");

        ImageView image = (ImageView) convertView.findViewById(R.id.venue_image);
        imageLoader.displayImage(objects.get(position).getImageUrl(), image, options);
        TextView header = (TextView) convertView.findViewById(R.id.venue_name);
        header.setText(objects.get(position).getName());

        TextView desc = (TextView) convertView.findViewById(R.id.venue_desc);
        desc.setText(objects.get(position).getDescription());
        desc.setTypeface(rLight);

        return convertView;
    }
}
