package com.mdev.spleattest.commons;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author Miral Desai on 04/2015.
 */
public class Utils {

    /**
     * Check to see if the the device is connected to the internet.
     *
     * @param c Context
     * @return true if connection is valid.
     */
    public static boolean checkNow(Context c) {

        ConnectivityManager cManager;
        NetworkInfo wifi_info, mobile_info;

        try {
            cManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            wifi_info = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            mobile_info = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (wifi_info.isConnected() || mobile_info.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            if (Constants.DEBUG) Log.d("Conn", e.getMessage());
        }
        return false;
    }
}
