package com.mdev.spleattest.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mdev.spleattest.collection.Data;
import com.mdev.spleattest.commons.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author Miral Desai on 04/2015.
 */
public class HttpTask extends AsyncTask<Void, Void, ArrayList<Data>> {

    private static final String TAG = "HttpTask";

    InputStream is = null;
    StringBuilder sb = null;
    String result = "";
    int statusCode;
    Context context;
    ProgressDialog mPb;

    /**
     * @param context Asking for context so that it can be passed to ProgressDialog
     */
    public HttpTask(Context context) {
        this.context = context;
    }

    public interface onCompleteTask {
        void onPostCompleted(ArrayList<Data> itemList);
    }

    onCompleteTask mCompleteTask;

    public void setListener(onCompleteTask oncomplete) {
        this.mCompleteTask = oncomplete;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mPb = ProgressDialog.show(context, null, "Loading venues...", true, false, null);
    }

    @Override
    protected ArrayList<Data> doInBackground(Void... params) {

        //Post request
        try {
            URL url = new URL(Constants.VENUE_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000); //ms
            conn.setConnectTimeout(5000); //ms
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            statusCode = conn.getResponseCode();

            //Show status code in log so we can see if it fails or succeeds.
            if (Constants.DEBUG) {
                Log.d(TAG, "Http status code: " + statusCode);
            }
            is = conn.getInputStream();
        } catch (MalformedURLException e) {
            if (Constants.DEBUG) {
                Log.e(TAG, e.getMessage());
            }
        } catch (IOException ioe) {
            if (Constants.DEBUG) {
                Log.e(TAG, ioe.getMessage());
            }
        }

        //Convert to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
            if (Constants.DEBUG) Log.d(TAG+" Convert:", result);
        } catch (UnsupportedEncodingException uee) {
            if (Constants.DEBUG) Log.d(TAG, "Encoding exception, failed to convert "+uee.getMessage());
        } catch (IOException ioe) {
            if (Constants.DEBUG) Log.d(TAG, "IOException when converting " + ioe.getMessage());
        }

        ArrayList<Data> itemList = new ArrayList<>();

        //Parse JSON
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jArray = jsonObject.getJSONArray("venues");

            for (int i=0;i < jArray.length();i++) {
                Data item = new Data();
                JSONObject json_object = jArray.getJSONObject(i);
                item.setName(json_object.getString("name"));
                item.setLatitude(json_object.getDouble("latitude"));
                item.setLongitude(json_object.getDouble("longitude"));
                item.setImageUrl(json_object.getString("list_image"));
                item.setDescription(json_object.getString("description"));
                itemList.add(item);
            }
        } catch (JSONException je) {
            if (Constants.DEBUG) {
                Log.d(TAG, je.getMessage());
            }
        }

        return itemList;
    }

    @Override
    protected void onPostExecute(ArrayList<Data> itemList) {
        super.onPostExecute(itemList);
        if (mCompleteTask != null) {
            mPb.dismiss();
            mCompleteTask.onPostCompleted(itemList);
            if (Constants.DEBUG) {
                Log.d(TAG, "Post made it");
            }
        }
    }
}