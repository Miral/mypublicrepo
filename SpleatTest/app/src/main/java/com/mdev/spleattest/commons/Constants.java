package com.mdev.spleattest.commons;

import com.mdev.spleattest.BuildConfig;

/**
 * @author Miral Desai on 04/2015.
 */
public class Constants {

    /**
     * Check if in debug mode
     * @return true if in debug mode
     */
    public static final boolean DEBUG = BuildConfig.DEBUG;

    /**
     * URL for Venue API
     */
    public static final String VENUE_URL = "https://spleatdemo.herokuapp.com/api/venues";

    /**
     * Roboto Light font location
     */
    public static final String RLIGHT = "RobotoLight.ttf";
}
