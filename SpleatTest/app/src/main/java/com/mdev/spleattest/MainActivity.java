package com.mdev.spleattest;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ListView;

import com.mdev.spleattest.adapters.ListAdapter;
import com.mdev.spleattest.async.HttpTask;
import com.mdev.spleattest.collection.Data;
import com.mdev.spleattest.commons.Constants;
import com.mdev.spleattest.commons.Utils;

import java.util.ArrayList;

/**
 *
 * --------- WHAT I WOULD CHANGE IF THIS WERE A REAL APP ---------
 *
 * � Since we have latitude and longitude, I would show a marker on a Google Maps integrated fragment.
 * It's something I have done before, and can do again. Possibly also offer to open Google Maps for navigation support via
 * an Intent Action. This would allow the user complete the entire process within our app. Finding a venue, getting there
 * and then paying when needed.
 *
 * � I would continue to use the third party library I chose to load the images, it's a very good library and cuts down on
 * boiler plate code. I would also change the default configuration I have set it up with, as it is highly customisable, and
 * since the images are a key part of the app to give it life, I would play around with how to present them efficiently.
 * For example when to cache them.
 *
 * � I would consider moving the data over to be XML based rather than JSON, for one reason only, pagination. With big images, and
 * lots of data, it would be handy to only load 10 at a time, so the user does not waste data loading every single one and only
 * look at the first two. This is something I have done before, so would not be hard to implement from the Android side of things.
 * It would also mean that when expanding to other countries, we don't need to worry about how good their internet or data connection
 * is, because we are already efficiently loading the information to save mobile data regardless.
 *
 * � Work with the styles.xml files more. I rushed through this a bit style wise, but of course I could use this to get better control
 * as the app would grow to have multiple activities. It would also to interesting to experiment by offering users more than one
 * theme, possibly allowing them to change it via the settings. Maybe something like a Night mode if they are out for dinner
 * and don't want a bright screen. This is something I have worked on that allows theme to automatically switch to a 'night mode"
 * when the sun sets. This is done automatically based on timezone, obviously not key to the app, but a nice feature to allow choice.
 *
 * � Facebook SDK integration. I have a lot of experience with the Facebook SDK, and for the user, it's easy to interact with.
 * This could also generate some marketing for the app, as we can post to their wall, every time they pay with the app, or maybe
 * a facebook check-in at the venue they are at, give the venue some marketing and maybe more venues will want to be listed
 * within our app and we can grow that way.
 *
 * � Move location gathering to a service. This would let it work in the background, and give us control when we need to call
 * the users location. All the GPS methods can also stay in one place which keeps the code cleaner, and easier to read.
 * The service would allow us to get the users location when they start the app, for example when they are chosing whether to
 * login via the Facebook SDK or not, or if we present a splash screen to the user. The current setup means that
 * if the LocationManager cannot get a location, it will return null. Meaning the ap functionality would break. This is
 * because currently I am relying on GPS only, within a service, I could create a proper method, for looking at Network
 * locations and via getBestProvider() to ensure we never return null, and that so that we do not have to rely on GPS.
 *
 * -------
 *
 * I have to admit I love the idea of the app, and I have a lot of ideas because I thought about building the exact same thing.
 * I don't have experience in running a business but from the idea side, it's certainly something I am excited about since
 * it is an idea I have explored. I haven't gone ahead with it because it was one of many ideas on my list as a developer.
 *
 * @author Miral Desai on 04/2015.
 */
public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";

    ListView list;
    LocationManager lm;
    double longitude, latitude;
    SharedPreferences mPrefs;

    private final LocationListener locationListener = new LocationListener() {

        public void onLocationChanged(Location location) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch(status) {
                case LocationProvider.AVAILABLE:
                    if (Constants.DEBUG) Log.d(TAG, "Status: Available");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    if (Constants.DEBUG) Log.d(TAG, "Status: Out of service");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    if (Constants.DEBUG) Log.d(TAG, "Status: Temp Unavailable");
                    break;
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (Constants.DEBUG) Log.d(TAG, "Provider: Disabled");
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (Constants.DEBUG) Log.d(TAG, "Provider: Enabled");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        list = (ListView) findViewById(R.id.list_main);
        getData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get Location data and save to sharedPrefs? Or Maybe use splash screen.
        //Splash screen can start service, get location
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        lm.removeUpdates(locationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
    }

    /**
     * Check internet connection on device, and query AsyncTask to start retrieving data.
     */
    private void getData() {
        boolean conn = Utils.checkNow(this);
        if (conn) {
            //Connected to internet, proceed with AsyncTask
            HttpTask httpTask = new HttpTask(this);
            httpTask.setListener(new HttpTask.onCompleteTask() {
                @Override
                public void onPostCompleted(ArrayList<Data> itemList) {
                    ListAdapter listAdapter = new ListAdapter(MainActivity.this, itemList, latitude, longitude);
                    list.setAdapter(listAdapter);
                }
            });
            httpTask.execute();
        } else {
            //No internet connection, prompt user with dialog.
        }
    }


}
