package com.mdev.spleattest.collection;

/**
 * @author Miral Desai on 04/2015.
 */
public class Data {

    private String name, short_name, address, postcode, phone,
                   id, description, status,
                   venue_type, image_url, max_tab_distance,
                   min_grat, country_code;

    private double latitude, longitude;

    //Getter and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return short_name;
    }

    public void setShortName(String short_name) {
        this.short_name = short_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVenueType() {
        return venue_type;
    }

    public void setVenueType(String venue_type) {
        this.venue_type = venue_type;
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }

    public String getMaxTabDistance() {
        return max_tab_distance;
    }

    public void setMaxTabDistance(String max_tab_distance) {
        this.max_tab_distance = max_tab_distance;
    }

    public String getMinGrat() {
        return min_grat;
    }

    public void setMinGrat(String min_grat) {
        this.min_grat = min_grat;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void setCountryCode(String country_code) {
        this.country_code = country_code;
    }
}
